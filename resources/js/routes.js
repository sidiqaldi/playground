export default [
    {path: '/', redirect: '/configs'},
  
    {
        path: '/about',
        name: 'about',
        component: require('./screens/about/index').default
    },
    {
        path: '/taiks',
        name: 'taiks',
        component: require('./screens/taiks/index').default
    },
    {
        path: '/configs',
        name: 'configs',
        component: require('./screens/configs/index').default
    },
];
