<?php

namespace Sidiqaldi\Playground\Commands;

use Illuminate\Console\Command;
use Sidiqaldi\Playground\PgConfig;
use Sidiqaldi\Playground\Services\ConfigService;

class PgConfigCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'playground:set-config {name} {value}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update playground configs by command';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(ConfigService $config)
    {
        $config->updateConfig($this->argument('name'), $this->argument('value'));
        $this->info("config {$this->argument('name')} updated!");
    }
}
