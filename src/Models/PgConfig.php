<?php

namespace Sidiqaldi\Playground;

use Illuminate\Database\Eloquent\Model;

class PgConfig extends Model
{
    protected $guarded = [];

    public static function sampleAction()
    {
        return "this sample action";
    }
}
