<?php

namespace Sidiqaldi\Playground;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Sidiqaldi\Playground\Commands\PgConfigCommand;
use Sidiqaldi\Playground\Services\ConfigService;

class PlaygroundServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ConfigService::class, function ($app) {
            return new ConfigService();
        });
    }

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                PgConfigCommand::class,
            ]);
        }

        $this->registerRoutes();
        $this->registerPublishing();
        $this->loadMigrationsFrom(__DIR__ . '/migrations/2018_08_31_021527_create_pg_configs_table.php');
        $this->loadViewsFrom(
            __DIR__.'/../resources/views', 'playground'
        );
    }

    /**
     * Register the package routes.
     *
     * @return void
     */
    private function registerRoutes()
    {
        Route::group($this->routeConfiguration(), function () {
            $this->loadRoutesFrom(__DIR__ . '/Http/routes.php');
        });
    }

    /**
     * Get the Telescope route group configuration array.
     *
     * @return array
     */
    private function routeConfiguration()
    {
        return [
            'namespace' => 'Sidiqaldi\Playground\Http\Controllers',
            'prefix' => config('playground.path'),
        ];
    }

    /**
     * Register the package's publishable resources.
     *
     * @return void
     */
    private function registerPublishing()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/migrations' => database_path('migrations'),
            ], 'playground-migrations');

            $this->publishes([
                __DIR__.'/../public' => public_path('vendor/playground'),
            ], 'playground-assets');

            $this->publishes([
                __DIR__.'/../config/playground.php' => config_path('playground.php'),
            ], 'playground-config');
        }
    }
}
