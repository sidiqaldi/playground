<?php

namespace Sidiqaldi\Playground;

class Playground
{
    public static function scriptVariables()
    {
        return [
            'path' => config('playground.path'),
            'timezone' => config('app.timezone'),
        ];
    }
}