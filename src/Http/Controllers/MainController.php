<?php

namespace Sidiqaldi\Playground\Http\Controllers;

use App\Http\Controllers\Controller;
use Sidiqaldi\Playground\Playground;

class MainController extends Controller
{
    public function index()
    {
        return view('playground::layout', [
            'cssFile' => 'app.css',
            'playgroundScriptVariables' => Playground::scriptVariables()
        ]);
    }
}