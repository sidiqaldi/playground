<?php

namespace Sidiqaldi\Playground\Http\Controllers;

use App\Http\Controllers\Controller;
use Sidiqaldi\Playground\PgConfig;
use Sidiqaldi\Playground\Http\Resources\PgConfig as PgConfigResource;

class ConfigController extends Controller
{
    public function index()
    {
        return PgConfigResource::collection(PgConfig::paginate(config('playground.pagination')));
    }
}