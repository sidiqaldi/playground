<?php

namespace Sidiqaldi\Playground\Services;

use Illuminate\Support\Facades\Cache;
use Sidiqaldi\Playground\PgConfig;

class ConfigService
{
    protected $configs;

    /**
     * Construct
     */
    public function __construct()
    {
        $this->configs = new PgConfig();
    }

    /**
     * Get config from cache first then check it from db
     *
     * @param string $name
     * @param [type] $default
     * @return void
     */
    public function getConfig(string $name, $default = null)
    {
        return Cache::get($name) ?? $this->getDbConfig($name, $default);
    }

    /**
     * Get config from db
     *
     * @param string $name
     * @param [type] $default
     * @return void
     */
    public function getDbConfig(string $name, $default = null)
    {
        $config = $this->configs->where('name', $name)->first()->value ?? $default;
        $this->updateCache($name, $config);
        return $config;
    }

    /**
     * Update config cache
     *
     * @param [type] $name
     * @param [type] $config
     * @return void
     */
    public function updateCache($name, $config)
    {
        if ($config) {
            Cache::put($name, $config, 180);
        }
    }

    /**
     * Update db config then update cache
     *
     * @param string $name
     * @param [type] $value
     * @return void
     */
    public function updateConfig(string $name, $value = null)
    {
        $config = $this->configs->firstOrNew(['name' => $name]);
        $config->value = $value;
        $config->save();

        Cache::put($name, $value, 180);
    }

    public function sampleAction()
    {
        return $this->configs->first();
    }

    public function anotherAction()
    {
        return $this->configs->sampleAction();
    }
}
